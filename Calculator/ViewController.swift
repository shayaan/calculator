//
//  ViewController.swift
//  Calculator
//
//  Created by Shayaan Siddiqui on 6/13/17.
//  Copyright © 2017 OneOrangeTree LLC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var display: UILabel!
    
    var userIsInTheMiddleOfTyping = false
    var periodPressed: NSMutableArray = NSMutableArray()
    
    var displayValue: Double {
        get {
            return Double(display.text!)!
        }
        
        set {
            display.text = String(newValue)//formatCurrency(value: newValue)
        }
    }
    
    private var brain = CalculatorBrain()
    
    func formatCurrency(value: Double) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.alwaysShowsDecimalSeparator = false
        formatter.decimalSeparator = "."
        formatter.maximumFractionDigits = 2;
        formatter.locale = Locale(identifier: Locale.current.identifier)
        let result = formatter.string(from: value as NSNumber)!;
        return result;
    }
    
    @IBAction func touchDigit(_ sender: UIButton) {
        let digit = sender.currentTitle!
        if userIsInTheMiddleOfTyping {
            if digit.contains(".") {
                periodPressed.add(digit)
                if periodPressed.count == 1 {
                    let textCurrentlyInDisplay = display.text!
                    display.text = textCurrentlyInDisplay + digit
                }
            } else {
                let textCurrentlyInDisplay = display.text!
                display.text = textCurrentlyInDisplay + digit
            }
        } else {
            if digit.contains(".") {
                periodPressed.add(digit)
            }
            display.text = digit
            userIsInTheMiddleOfTyping = true
        }
    }
    
    @IBAction func performOperation(_ sender: UIButton) {
        if userIsInTheMiddleOfTyping {
            brain.setOperand(displayValue)
            userIsInTheMiddleOfTyping = false
        }
        if let mathematicalSymbol = sender.currentTitle
        {
            brain.performOperation(mathematicalSymbol)
        }
        
        if let result = brain.result {
            displayValue = result
            display.text! = formatCurrency(value: displayValue)
        }
        periodPressed.removeAllObjects()
    }
}

